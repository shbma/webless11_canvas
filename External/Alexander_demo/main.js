
!function(Math){
	var cnv = $('#cnv')[0];
	if(!cnv)return;
	
	var ctx     = cnv.getContext('2d');

	var app = {
		init:function( map , l ){
			var t=this;
			t.width	 = t.height = t.size*l;
			t.l = l;
			t.map = map;
			t.show();
			return t;
		},
		color	:	{
			fon     :	'#ddd',
			stena	:	'#f00',
			cele	:	'#0dd',
			heroi	:	'#7aa',
		},
		
		size	:	50,
		width	:	null,
		height	:	null,
		
                show_Rect:function(ctx,i,color){
			var t=this, s = t.size, y=Math.floor( i/t.l), x=i-y*t.l;
			
			ctx.fillStyle = t.color[color]; 
			ctx.fillRect( x*s, y*s, s, s);
			return 
		},
		
		show:function(){
			var t=this,i=t.map.length,tmp;
			ctx.fillStyle = t.color.fon;
                        ctx.fillRect(0, 0, t.width, t.height);
			
			for(;i--;){
				tmp = t.map[i];
				if(!tmp){
					t.show_Rect(ctx,i,'stena');
				}else{
					tmp = (32+tmp).toString(2);
					if(tmp[4]==1) t.show_Rect(ctx,i,'heroi');
					if(tmp[3]==1) t.show_Rect(ctx,i,'cele'); 
				}
			}
			return this;
		},
		
		up	: function(){},
		down: function(){},
		left: function(){},
		right: function(){},
		
	};
	app.init( 
			[	0, 0, 0, 0, 0, 0,
				0, 3, 1, 1, 1, 0,
				0, 0, 0, 0, 1, 0,
				0, 1, 1, 1, 1, 0,
				0, 1, 0, 0, 0, 0,
				0, 1, 1, 1, 5, 0,
				0, 0, 0, 0, 0, 0,
			]
		, 6 );
}(Math);