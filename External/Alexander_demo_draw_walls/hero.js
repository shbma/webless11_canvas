/* 
 * hero object
 */

var hero = {
    type        :   3,  //это активная сущность    
    track       :   [], //маршрут по точкам    
    target      :   {   //цель
                type : 5, // тип цели
                pos: [null,null]   // координаты цели
    },
    level       :   0,  //для конфликтов при встрече

    init:function(parent, beg_point, lev){
        var t = this;
        t.track = [[].concat(beg_point), [].concat(beg_point)];               
        t.level = lev;
        t.parent = parent;
        
        if (t.find_target()){  
            console.log('target found at: ' + t.target.pos);
            t.make_track();
        };
        
        return t;
    },

    find_target: function(){       
        /*найти координаты цели (по ее типу) на карте*/
        var t=this, mp=t.parent.map;
        for(var lin in mp){ //y           
            var xindex = mp[lin].indexOf(t.target.type);            
            if (xindex+1){ 
                t.target.pos = [].concat([xindex,lin]);
                return true;
            }
        }
        return false;
    },

    target_is_reached: function(){
        //критерий достижения цели, пересчитываемый каждый шаг        
        t=this;
        if ( (t.target.pos[0] === t.track[1][0]) && (t.target.pos[1] === t.track[1][1]) ){
            return true;
        } else{ 
            return false;
        }
    },
    
    make_track: function(){
        /*проложить маршрут до цели по ее координатам*/
        var t=this;
        console.log('making_track from '+t.track[1] + ' to ' + t.target.pos);
        //0,0 - top left
        var matrix = t.clone_map();                
        var grid = new PF.Grid(matrix[0].length, matrix.length, matrix);
        var finder = new PF.AStarFinder();
        console.log(t.track[1][0], t.track[1][1],t.target.pos[0], t.target.pos[1]);
        
        var history = [].concat(t.track[0]); //храним предыдущую точку
        t.track = finder.findPath( t.track[1][0], t.track[1][1], 
                                    t.target.pos[0], t.target.pos[1], 
                                    grid);        
        t.track.unshift(history); 
        
        console.log(t.track);
    },
    
    clone_map: function(){
        /* возвращает клон глобальной карты в "черно-белом" виде: в ячейках
         * 1 - везде, куда идти нельзя(по любым соображениям), 0 - можно*/
        var t=this;
        var mp=t.parent.map;        
        var new_map = Array();
        
        for(var ln in mp){
            var new_ln = Array();
            for(var e in mp[ln]){
                new_ln.push(t.cell_salt(mp[ln][e]));
            }            
            new_map.push(new_ln);            
        }
        
        return new_map;
    },
    
    cell_salt:function(typ){
        /*стоит-ли идти через эту ячейку*/
        if (typ == 1) { //стена
            return 1;
        }else{
            return 0;
        }
    },
    
    do_track:function(){},

    do_step:function(){        
        console.log('stepping: ');
        var t = this;        
        if (t.track.length > 2) { //если есть куда шагать
            if (t.look_around(t.track[2])) { //осматриваемся, если вокруг спокойно - шагаем                
                //запрашиваем перемещение героя по карте                            
                if (t.parent.move_cell(t, t.track[1], t.track[2])) { 
                    // карта дала добро и поменялась                    
                    t.track.shift(); //стираем неактуальную историю
                    console.log('after unshift: ');
                    console.log(t.track);
                } 
            }
        }
                                                         
    },

    look_around:function(targ){
        /*проверка, безопасно ли шагнуть на указанные координаты*/
        console.log('looking around for ' + targ);
        
        return true;

    },
    
    show:function(){
        var t=this, s = t.parent.size;
			
        t.parent.ctx.fillStyle = '#0F0'; 
        t.parent.ctx.fillRect( t.track[1][0]*s, t.track[1][1]*s, s, s);        
    }
};

