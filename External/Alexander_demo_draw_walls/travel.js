
!function(Math){
	var cnv = $('#cnv')[0];
	if(!cnv)return;
	              
	var nature = {
		init:function( map ){                        
			var t=this;
                        
                        t.ctx = cnv.getContext('2d');
                        
			t.mver = map.length; // клеток по вертикали
                        t.mhor = map[0].length; // клеток по горизонатали
			t.width	 = t.size*t.mhor;
                        t.height = t.size*t.mver;
                        t.map = map;                       
			t.man = $.extend(true,{},hero).init(t, [1,1], 0); // порождаем персонажа
                        
                        t.zoo = [t.man] // коллекция ссылок на героев
                        
                        //соответствие клавиш и единичных векторов перемещения
                        t.e_vects = {38:[0,-1], /*up*/   40:[0,1], /*down*/
                                     37:[-1,0], /*left*/ 39:[1,0]  /*right*/}
                        
                        $(document).keyup(function(event){ //клавиши -> в движение
                            if (event.keyCode in t.e_vects) {
                                //переопределить координаты цели
                                var new_tar = [
                                    t.man.track[1][0] + t.e_vects[event.keyCode][0],
                                    t.man.track[1][1] + t.e_vects[event.keyCode][1],
                                ]
                                if (!t.is_wall(new_tar)){ //если не ломимся в стену
                                    t.man.target.pos = [].concat(new_tar);
                                    //пересчитать маршрут
                                    t.man.make_track();
                                    //шагать
                                    t.man.do_step(); 
                                    t.show();                                                   
                                }                                
                                
                            }                            
                        });
                        
                        //демо самостоятльного прохождения маршрута
                        $(document).click(function(){
                            setInterval(function(){
                                if (t.man.find_target()){
                                    t.man.make_track();
                                    if (!t.man.target_is_reached()) {
                                        t.man.do_step();                                         
                                    }
                                }
                                t.show();                                                   
                            },500);
                        });
                        
			t.loadImage(function(){
				t.show();   
			});			
			return t;
		},
		_stena:null,
		loadImage:function(fn){
			var t=this;
			var stena = new Image();
			stena.onload = function(){
				fn()
			};

			stena.src = 'img/wall.png';
			t._stena=stena;
		},
		
		color	:	{
			fon     :	'#ddd',
			stena	:	'#eee',
			cele	:	'#0dd',			
		},
		                                
		size	:	50,
		width	:	null,
		height	:	null,
		
        show_Rect:function(mx,my,color){
			var t=this, s = t.size;
			if (color=='stena'){
				t.ctx.drawImage(t._stena, mx*s, my*s, s, s)
			}
			else{
			t.ctx.fillStyle = t.color[color]; 
			t.ctx.fillRect( mx*s, my*s, s, s);
			}
			return 
		},
		
		show:function(){
			var t=this, tmp;
			t.ctx.fillStyle = t.color.fon;
                        t.ctx.fillRect(0, 0, t.width, t.height);
			
			for(var i=t.map.length;i--;) //y
                            for(var j=t.map[0].length;j--;) //x
                                {
                                    tmp = t.map[i][j];
                                    if(tmp==1){
                                            t.show_Rect(j,i,'stena');
                                    }else{
                                            tmp = (32+tmp).toString(2);
                                            if(tmp[4]==1) {
                                                for(var k in t.zoo){
                                                   t.zoo[k].show(); 
                                                }                                               
                                            }
                                            if(tmp[3]==1) t.show_Rect(j,i,'cele'); 
                                    }
                                }
			return this;
		},
                
                move_cell:function(who,from,to){
                    /*двигаем объект, если можно*/
                    console.log('request for moving processing');
                    var t = this;
                    
                    if (!t.is_wall(to)){
                        //двигаем объект who
                        t.map[to[1]][to[0]] = who.type;
                        //зачищаем место, где он стоял
                        t.map[from[1]][from[0]] = 0; 
                        console.log('moving approved')
                        return true;  //успешно
                    }else{
                        return false; //никуда не пошли 
                    }
                },
                
                is_wall:function(targ){
                    /*проверка координат - а не в стене ли они?*/
                    var t = this;
            
                    if ( (targ[0] >= 0 && targ[0] <= t.mhor - 1) && 
                        (targ[1] >= 0 && targ[1] <= t.mver - 1)){
                           var tmp = t.map[targ[1]][targ[0]];
                           console.log(tmp);
                           if(tmp == 1){ //wall
                                   return true;
                           }else{
                                   return false;
                           }
                   } else {
                       return true;
                   }
                }
		
	};
        //1 - wall
	nature.init([
                        [1, 1, 1, 1, 1, 1],
                        [1, 3, 0, 0, 0, 1],
                        [1, 1, 0, 1, 0, 1],
                        [1, 0, 0, 0, 0, 1],
                        [1, 0, 1, 1, 1, 1],
                        [1, 0, 0, 0, 5, 1],
                        [1, 1, 1, 1, 1, 1],
                    ]
		);        
}(Math);



