/* 
 * hero and target objects
 */

/*конструктор целей*/
var Target = function(arg){
    var t=this;
    t.id = (typeof(arg['id']) === "undefined") ? 5 : arg['id'];//id цели
    if (typeof(arg['pos']) !== "undefined") t.pos = arg['pos'] //массив координат цели e.g. [2,4]
}

/*объект Созданье - прототип для героев и монстров*/
var Creature = function(){};        

Creature.prototype.init = function(parent, beg_point, beg_targ_id, lev, tim, ipath){
    var t = this;

    t.id          =   Math.floor(Math.random()*100000),//персональный идентификатор
    t.team        =   tim, //команда
    t.track       =   [],  //маршрут по точкам    
    t.targets     =   [],  //очередь целей    
    t.level       =   lev, //сила - для конфликтов при встрече
    t.img         =   null, //спрайт

    t.loadImage(ipath); // подгрузим и сохраним фотку-спрайт

    t.track = [[].concat(beg_point), [].concat(beg_point)];               

    t.parent = parent;
    t.targets = [new Target({'id':beg_targ_id})];

    if (t.findTarget()){  
        console.log('target found at: ' + t.targets[0].pos);
        t.makeTrack();
    };

    return t;
};

Creature.prototype.findTarget = function(){       
    /*найти координаты цели (по ее id) на карте*/        
    var t=this, mp=t.parent.map;
    if (t.targets.length){ //если есть хотя бы одна цель
        for(var lin in mp){ //y           
            var xindex = mp[lin].indexOf(t.targets[0].id);            
            if (xindex+1){ 
                t.targets[0].pos = [].concat([xindex,Number(lin)]);
                return true;
            }
        }
    }
    //если не нашли - выкидываем из списка целей
    t.targets.shift();
    return false;
};

Creature.prototype.targetIsReached = function(){
    //критерий достижения цели, пересчитываемый каждый шаг        
    t=this;
    if ( t.targets.length && (t.targets[0].pos[0] === t.track[1][0]) 
         && (t.targets[0].pos[1] === t.track[1][1]) ){
        t.targets.shift(); //вычеркнули            
        if (t.targets.length === 0) {
            return true; // целей больше не осталось
        }
    } else{ 
        return false;
    }
};

Creature.prototype.makeTrack = function(){
    /*проложить маршрут до цели по ее координатам*/
    var t=this;
    console.log('making_track from '+t.track[1] + ' to ' + t.targets[0].pos);
    //0,0 - top left
    var matrix = t.cloneMap();                
    var grid = new PF.Grid(matrix[0].length, matrix.length, matrix);
    var finder = new PF.AStarFinder();
    console.log(t.track[1][0], t.track[1][1],t.targets[0].pos[0], t.targets[0].pos[1]);

    var history = [].concat(t.track[0]); //храним предыдущую точку
    t.track = finder.findPath( t.track[1][0], t.track[1][1], 
                                t.targets[0].pos[0], t.targets[0].pos[1], 
                                grid);        
    t.track.unshift(history); 

    console.log(t.track);
};

Creature.prototype.cloneMap = function(){
    /* возвращает клон глобальной карты в "черно-белом" виде: в ячейках
     * 1 - везде, куда идти нельзя(по любым соображениям), 0 - можно*/
    var t=this;
    var mp=t.parent.map;        
    var new_map = Array();

    for(var ln in mp){
        var new_ln = Array();
        for(var e in mp[ln]){
            new_ln.push(t.cellSalt(mp[ln][e]));
        }            
        new_map.push(new_ln);            
    }

    return new_map;
};

Creature.prototype.cellSalt = function(typ){
    /*стоит-ли идти через эту ячейку*/
    if (typ == 1) { //стена
        return 1;
    }else{
        return 0;
    }
};


Creature.prototype.getNextStep = function(){
    /*возвращает желаемые координаты следующего шага*/
    console.log('planned nex step: ');
    var t = this;        

    if (t.track.length > 2) { //если есть куда шагать
        if (t.lookAround(t.track[2])) { //осматриваемся, если вокруг спокойно                 
            return t.track[2];           //сообщаем желаемые координаты     
        } 
    }  
    return false;
};

Creature.prototype.clearHistory = function(){
    var t=this;

    t.track.shift(); //стираем неактуальную историю
    console.log('after unshift: ');
    console.log(t.track);
};

Creature.prototype.lookAround = function(targ){
    /*проверка, безопасно ли шагнуть на указанные координаты*/
    console.log('looking around for ' + targ);

    return true;

};

Creature.prototype.die = function(){
    alert('Существо с id='+this.id+' съели...');
};

Creature.prototype.loadImage = function(path,fnback){
    /*path - путь к спрайту, fn - вызывается после загрузки*/
    if (typeof(path) !== 'undefined'){
        var t=this;
        var loc_img = new Image();
        if (typeof(fnback) !== 'undefined'){
            loc_img.onload = function(){
                fnback();
            };
        }        

        loc_img.src = path;//'img/wall.png';
        t.img=loc_img;            

        return true;
    } else {
        return false;
    }
},

/*this.show  = function(){
    var t=this, s = t.parent.size;

    t.parent.ctx.fillStyle = '#0F0'; 
    t.parent.ctx.fillRect( t.track[1][0]*s, t.track[1][1]*s, s, s);        
};*/

Creature.prototype.clear = function(x, y){
    /*очистка конкретной яейки*/
    var t = this, s = t.parent.size;

    t.parent.ctx.clearRect(x, y, s, s);
    t.parent.ctx.fillStyle = t.parent.color.fon; 
},

Creature.prototype.momentDraw = function(x_old, y_old, x_new, y_new){
    /*отрисовка конкретного кадра в процессе перемещения*/
    var t = this, s = t.parent.size;

    t.parent.ctx.clearRect(x_old, y_old, s, s);
    t.parent.ctx.fillStyle = t.parent.color.fon; 
    t.parent.ctx.fillRect( x_old, y_old, s, s);        
    t.parent.ctx.drawImage(t.img, x_new, y_new, s, s);
},

Creature.prototype.show  = function(){
    var t=this, 
        tv = t.parent.viewport,
        nframes = 5,
        s = t.parent.size;
    
    //console.log('рисую ' + this.team);
    //console.log('рисую ' + this.track);
    
    var step = Math.round(s/nframes),
        xi_prev = (t.track[0][0] - tv.x)*s, yi_prev = (t.track[0][1] - tv.y)*s,
        xi = (t.track[1][0] - tv.x)*s, yi = (t.track[1][1] - tv.y)*s;
            
    if (typeof(t.track[2]) !== 'undefined') {
        var dir = [ t.track[2][0] - t.track[1][0], t.track[2][1] - t.track[1][1] ];

        var smoothMove = setInterval(function(){

            t.momentDraw(xi_prev, yi_prev,xi, yi);

            xi_prev = xi; yi_prev = yi;
            xi += step*dir[0]; yi += step*dir[1];

            if (nframes-- <= 0) clearInterval(smoothMove);

          },Math.floor(t.parent.timeint/(nframes+1)));

    } else {    
        t.momentDraw(xi, yi, (t.track[1][0]-tv.x)*s, (t.track[1][1]-tv.y)*s);        
    };

};
    


creature = new Creature(); /*создали объект-прототип*/

/*конструктор Героя*/
var Hero = function(){};
Hero.prototype = creature;

/*конструктор Монстра*/
var Monster = function(){};
Monster.prototype = creature;


