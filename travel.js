var ConflictCell = function(pos,nums,zu){
    //несет инф-ю о ячейке и zoo-номерах персонажей, желающих ее занять
    
    this.pos = pos;  //массив [x,y]
    this.no = nums;  // массив номеров  [1,40,0,39,...]
    this.zuu = zu;    //ссылка на зоопарк, из которого номера в this.no
};    

ConflictCell.prototype.pushNo = function(n){
    var t=this;        
    t.no.push(n);
};

ConflictCell.prototype.getWinnerNo = function(){
    /*возвращает номер персонажа (из спорящих) с наибольшим уровнем*/
    var t=this;

    var max = {val:0,idx:0};
    for (var i in t.no){
        if (t.zuu[t.no[i]].level > max.val) {
            max.val = t.zuu[t.no[i]].level;
            max.idx = i;
        }
    }

    return max.idx;
};
    


var ConflictMap = function(){
    //массив конфликтных ячеек с методами к ним относящимися
    
    this.list = []; //список конфликтных ячеек
};    

ConflictMap.prototype.push = function(cel){
    this.list.push(cel);
};

ConflictMap.prototype.clear = function(){
    this.list = [];        
};

ConflictMap.prototype.pointInside = function(npos){
    /*есть ли уже точка с такими координатами в конфликтном массиве*/
    var t = this;

    for(var j in t.list){
        if (t.arraysEqual(npos,t.list[j].pos)) 
            return j;
    }

    return -1;
};
    
ConflictMap.prototype.arraysEqual = function (a, b) {
    /*определяет равенство массивов*/
    if (a === b) return true;
    if (a === null || b === null) return false;
    if (a.length !== b.length) return false;

    for (var i = 0; i < a.length; ++i) {
      if (a[i] !== b[i]) return false;
    }
    return true;
};


!function(Math){
	var cnv = $('#cnv')[0];
	if(!cnv)return;
	              
	var nature = {
		init:function( map ){                        
			var t=this;
                        
                        t.ctx = cnv.getContext('2d');
                                                                        
			t.mver = map.length; // клеток по вертикали
                        t.mhor = map[0].length; // клеток по горизонатали
			t.width	 = t.size*t.mhor;
                        t.height = t.size*t.mver;
                        t.timeint = 500; //интервал для setInterval осн.цикла
                        t.map = map;    
                        t.conflict_map = new ConflictMap(); //координаты и zoo-номера жалающих на них
                        t.zoo = [];  //коллекция всех действующих лиц
                        
                        t.man = new Hero();
                        //t.man.init(t, [2,1], 5 ,0, 0,'img/ch_l_0.png'); // порождаем человека                        
                        t.man.init(t, [2,1], 5 ,0, 0,'img/move_man.png'); // порождаем человека                        
                        t.zoo.push(t.man); // вводим в коллектив
                        t.populateMap(); //подселяем на карту
                        
                        t.ogre = new Monster();
                        t.ogre.init(t, [4,1], t.man.id ,1, 1,'img/ogr.png'); // порождаем огра                        
                        t.zoo.push(t.ogre); //вводим в коллектив                                                
                        t.populateMap(); //подселяем на карту
                        
                        t.setControls(); // задаем механику управления героем
                       
                        //окно просмотра
                        t.viewport.x =0; t.viewport.y = 0; 
                        t.viewport.width = t.map[0].length; 
                        t.viewport.height = t.map.length; // размеры в клетках
                       
                        t.showStatic(); // отрисовываем фон и пр. неподвижности
                        
                        /*Основной цикл*/
                        var intervalId = setInterval(function(){t.cycle.call(t);},t.timeint);
                            
			//t.show();                                                   
			return t;
		},
                   
                populateMap: function(){ /*сделать доп. возм-ть подселяь только одного*/
                    /*высавляем по карте персонажей*/
                    var t=this;
                    
                    for(var i in t.zoo){
                        t.map[t.zoo[i].track[1][1]][t.zoo[i].track[1][0]] = t.zoo[i].id;
                    }
                },
                
                cycle: function(){
                    var t=this;
                    
                    /*var her = t.zoo[0], tv = t.viewport;                    
                    if (her.track[1][1] > tv.y + tv.height - 1) {tv.y += 1;}
                    if (her.track[1][0] > tv.x + tv.width - 1) {tv.x += 1; }*/
                    
                    t.show();
                    
                    /* перебираем всех, спрашиваем куда хотят, 
                     * пишем карту с конфликтами*/                    
                    t.fillConflictMap();

                    /*решаем конфликты, правим карту - выживает сильнейший*/
                    t.solveConflicts();
                },                                
                
                fillConflictMap: function(){
                    /* перебираем всех действующих лиц, спрашиваем куда хотят, 
                     * пишем карту с конфликтами*/
                    var t=this, want_pos;
                    
                    for(var i in t.zoo){
                        if (t.zoo[i].findTarget()){
                            t.zoo[i].makeTrack();
                        }                           

                        want_pos = t.zoo[i].track[1]; // по-умолчанию

                        if (!t.zoo[i].targetIsReached()) { //цель еще впереди - смотрим куда собрался шагать
                            want_pos = t.zoo[i].getNextStep() ? t.zoo[i].getNextStep() : want_pos;
                        }  

                        console.log('i='+i+'want_pos='+want_pos)

                        var conf_i = t.conflict_map.pointInside(want_pos);                                    

                        //если координаты уже есть и для них такого героя еще нет
                        if ( (conf_i >= 0) && (t.conflict_map.list[conf_i].no.indexOf(i) < 0)){
                            t.conflict_map.list[conf_i].pushNo(i);                                        
                        } else{
                            t.conflict_map.push(new ConflictCell(want_pos,[i],t.zoo)); 
                        }

                        console.log('cmap was made, length='+t.conflict_map.list.length)
//                          console.log(t.conflict_map[0].no)
//                          console.log(t.conflict_map[1])
                             
                        
                    }
                },
        
                solveConflicts: function(){
                    /*решаем конфликты, правим карту - выживает сильнейший*/
                    var t=this, 
                        her; //персонаж
                    
                    for(var j in t.conflict_map.list){
                        console.log('j=' + j);
                        var scrum = t.conflict_map.list[j];
                        console.log('scrum.no = ' + scrum.no);
                        
                        if (scrum.no.length > 1){ // вот он, конфликт
                            //scrum.no.sort.call(scrum,t.compare_actors); //вопрос - не сортирует, вообще не заходит в compare_actors
                            var wi_no = scrum.getWinnerNo(); //
                                                        
                            her = t.zoo[wi_no]; //это самый крутой                            
                                                        
                            for(k in scrum.no){ // убиваем неудачников из других команд.
                                                // своих игнорируем
                                var weak = t.zoo[scrum.no[k]]; // при удалении (см.ниже) обращаемяся также впрямую, не через weak
                                
                                if (weak.team !== her.team) {
                                    t.map[weak.track[1][1]][weak.track[1][0]] = 0; //подчищаем карту
                                    weak.die();
                                    //delete weak; // удаляем - не удаляется - вопрос почему
                                    delete t.zoo[scrum.no[k]]; // удаляем
                                    console.log('объект удален в борьбе за клетку');
                                }
                            }
                            
                        } else{ //спорить не с кем - просто переставляем персонажа
                                                        
                            her = t.zoo[scrum.no[0]]; //наш единственный претендент
                            
                        }
                        
                        t.moveCell(her,her.track[1],scrum.pos); //пишем выжившего в карту                
                        
                        if ( (her.track[1][0] !== scrum.pos[0]) || (her.track[1][1] !== scrum.pos[1]) ){//если реально сместился                            
                            her.clearHistory(); // обновляем ему историю - смещаем внутр.указатель положения
                        };
                    }
                    
                    t.conflict_map.clear(); //очищаем массив конфликтов
                },
                
                setControls: function(){
                    // задаем механику управления героем - реакцию на клавиши, мышь
                    var t=this;
                    
                    //соответствие клавиш и единичных векторов перемещения
                    t.e_vects = {38:[0,-1], /*up*/   40:[0,1], /*down*/
                                 37:[-1,0], /*left*/ 39:[1,0]  /*right*/};

                    $(document).keyup(function(event){ //клавиши -> в движение
                        if (event.keyCode in t.e_vects) {
                            //переопределить координаты цели
                            var new_tar = [
                                t.man.track[1][0] + t.e_vects[event.keyCode][0],
                                t.man.track[1][1] + t.e_vects[event.keyCode][1]
                            ];
                            if (!t.isWall(new_tar)){ //если не ломимся в стену
                                //if (!t.man.targets.length){ //целей нет - добавим
                                    //t.man.targets.push(
                                    //        new Target({'id':7, 'pos':new_tar}));                                
                                t.man.targets.unshift(new Target({'id':7,'pos':new_tar}));                                    
                                //}
                                //t.man.targets[0].pos = [].concat(new_tar); 
                            }                                

                        }                            
                    });

                    //Управление мышкой через постановку промежуточных целей
                    $(cnv).click(function(e){                            
                        nx = Math.floor(
                                (e.pageX - $(this).offset().left) / t.size
                                );
                        ny = Math.floor(
                                (e.pageY - $(this).offset().top) / t.size
                                );                                                            
                        if ( (nx < t.mhor) && (ny < t.mver) && (!t.isWall([nx,ny])) ) {                                
                            t.map[ny][nx] = 7; //ставим флажок на карте
                            //добавляем приоритетную цель
                            t.man.targets.unshift(new Target({'id':7,'pos':[nx,ny]}));
                        }                            
                    });
                },                                                                
                
                viewport: {
                    x: 0, y: 0, width: 0, height: 0
                },
                
		color	:	{
			fon     :	'#ddd',
			stena	:	'#f00',
			cele	:	'#0dd'			
		},
		                                
		size	:	30,
		width	:	null,
		height	:	null,
		
                showRect:function(mx,my,color){
			var t=this, s = t.size;
			
			t.ctx.fillStyle = t.color[color]; 
			t.ctx.fillRect( mx*s, my*s, s, s);
			
		},
				                
                moveCell:function(who,from,to){
                    /*двигаем объект, если можно*/
                    console.log('request for moving processing from team '+who.team);
                    var t = this;
                    
                    if (!t.isWall(to)){
                        //двигаем объект who
                        t.map[to[1]][to[0]] = who.id;
                        //зачищаем место, где он стоял
                        t.map[from[1]][from[0]] = 0; 
                        console.log('moving approved');
                        return true;  //успешно
                    }else{
                        return false; //никуда не пошли 
                    }
                },
                
                isWall:function(targ){
                    /*проверка координат - а не в стене ли они?*/
                    var t = this;
            
                    if ( (targ[0] >= 0 && targ[0] <= t.mhor - 1) && 
                        (targ[1] >= 0 && targ[1] <= t.mver - 1)){
                           var tmp = t.map[targ[1]][targ[0]];
                           console.log(tmp);
                           if(tmp == 1){ //wall
                                   return true;
                           }else{
                                   return false;
                           }
                   } else {
                       return true;
                   }
                },              
                
                showStatic: function() {
                    var t=this;
                    
                    t.ctx.fillStyle = t.color.fon;
                    t.ctx.fillRect(0, 0, t.width, t.height);
                },
                
                inViewport: function(xi,yi) {
                    //смотрим, попадают ли координаты (в клетках) в окно просмотра
                    var tv = this.viewport;
                    
                    if (yi <= (tv.y + tv.height) && yi >= tv.y &&
                        xi <= (tv.x + tv.width) && xi >= tv.x){
                        return true;
                    } else {
                        return false;
                    }
                     
                },
                
                show:function(){
			var t=this, tmp;
                        var tv = t.viewport;
			//t.ctx.fillStyle = t.color.fon;
                        //t.ctx.fillRect(0, 0, t.width, t.height);
			
			for(var i=t.map.length;i--;) //y
                            for(var j=t.map[0].length;j--;) //x
                                {
                                    if (t.inViewport(j,i)){
                                            tmp = t.map[i][j];
                                            if (!tmp) {
                                                t.showRect(j - tv.x, i - tv.y,'fon');
                                            }
                                            if(tmp==1){
                                                t.showRect(j - tv.x, i - tv.y,'stena');
                                            }else{
                                                if ( (tmp == 5) || (tmp == 7) ){
                                                    t.showRect(j - tv.x, i - tv.y, 'cele'); 
                                                } else {
                                                    for(var k in t.zoo){
                                                        if (t.inViewport(t.zoo[k].track[1][0],t.zoo[k].track[1][1])){
                                                            t.zoo[k].show(); 
                                                        } else {
                                                            t.showRect(j - tv.x, i - tv.y,'fon'); 
                                                        }

                                                    }
                                                }
                                            }
                                    
                                        }
                                };
			return this;
		},
		
	};
        //0 - floor
        //1 - wall        
        //7 - user flag for man
	nature.init([
                        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                        [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1],
                        [1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1],
                        [1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
                        [1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1],
                        [1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1],
                        [1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1],
                        [1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1],
                        [1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1],
                        [1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1],
                        [1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1],
                        [1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 5, 1],
                        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                    ]
		);        
}(Math);



